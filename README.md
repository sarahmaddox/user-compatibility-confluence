# User Compatibility Pack for Confluence

## What it is?

Atlassian provides a compatibility library that your plugin can include so that it can interact with either v4.3 or v5.2 in a safe manner.

## When I need it?

If you want to be compatible with Confluence 4.3 and later, you cannot use `ConfluenceUser` class directly, as it was introduced in Confluence 5.2.

## How to use it?

Add a maven dependency by adding following to your `pom.xml`:

    <dependency>
        <groupId>com.atlassian.usercompatibility</groupId>
        <artifactId>usercompatibility-confluence</artifactId>
        <version>1.0</version>
    </dependency>

