package com.atlassian.confluence.usercompatibility;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import com.atlassian.user.impl.DefaultUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserCompatibilityHelperTestWORename
{
    private static final User FRED = new DefaultUser("fred");

    @Mock private ContainerContext containerContext;
    @Mock private UserAccessor userAccessor;

    @Before
    public void setUp()
    {
        ContainerManager.getInstance().setContainerContext(containerContext);
        when(containerContext.getComponent("userAccessor")).thenReturn(userAccessor);
    }

    @Test
    public void testUserRenameIsNotSupported() throws Exception
    {
        assertFalse(UserCompatibilityHelper.isRenameUserImplemented());
    }

    @Test
    public void testGetKeyForUserReturnsUsername() throws Exception
    {
        assertEquals(FRED.getName(), UserCompatibilityHelper.getKeyForUser(FRED));
    }

    @Test
    public void testGetKeyForUserReturnsNullForNullUser() throws Exception
    {
        assertNull(UserCompatibilityHelper.getKeyForUser(null));
    }

    @Test
    public void testGetUserForKeyTakesUsername() throws Exception
    {
        when(userAccessor.getUser(FRED.getName())).thenReturn(FRED);
        assertEquals(FRED, UserCompatibilityHelper.getUserForKey(FRED.getName()));
    }

    @Test
    public void testGetUserForKeyReturnsNullForNullKey() throws Exception
    {
        assertNull(UserCompatibilityHelper.getUserForKey(null));
    }
}