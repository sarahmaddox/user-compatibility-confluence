package com.atlassian.confluence.usercompatibility;

import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.persistence.dao.ConfluenceUserDao;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.impl.DefaultUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserCompatibilityHelperTestWithRename
{
    @Mock private ContainerContext containerContext;
    @Mock private UserAccessor userAccessor;
    @Mock private ConfluenceUserDao confluenceUserDao;
    @Mock private ConfluenceUser fred;

    @Before
    public void setUp()
    {
        ContainerManager.getInstance().setContainerContext(containerContext);
        when(containerContext.getComponent("userAccessor")).thenReturn(userAccessor);
        when(containerContext.getComponent("confluenceUserDao")).thenReturn(confluenceUserDao);
        when(fred.getKey()).thenReturn("uuid");
        when(fred.getName()).thenReturn("fred");
    }

    @Test
    public void testUserRenameIsSupported() throws Exception
    {
        assertTrue(UserCompatibilityHelper.isRenameUserImplemented());
    }

    @Test
    public void testGetKeyForUserReturnsUserId() throws Exception
    {
        assertEquals(fred.getKey(), UserCompatibilityHelper.getKeyForUser(fred));
    }

    @Test
    public void testGetKeyForUserReturnsUserIdForNonConfluenceUsers() throws Exception
    {
        when(userAccessor.getUser(fred.getName())).thenReturn(fred);
        assertEquals(fred.getKey(), UserCompatibilityHelper.getKeyForUser(new DefaultUser(fred.getName())));
    }

    @Test
    public void testGetKeyForUserReturnsNullForNullUser() throws Exception
    {
        assertNull(UserCompatibilityHelper.getKeyForUser(null));
    }

    @Test
    public void testGetUserForKeyTakesUserId() throws Exception
    {
        when(confluenceUserDao.findByKey(fred.getKey())).thenReturn(fred);
        assertEquals(fred, UserCompatibilityHelper.getUserForKey(fred.getKey()));
    }

    @Test
    public void testGetUserForKeyReturnsNullForNullKey() throws Exception
    {
        assertNull(UserCompatibilityHelper.getUserForKey(null));
    }
}
