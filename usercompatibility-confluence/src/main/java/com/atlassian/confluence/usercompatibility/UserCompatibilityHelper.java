package com.atlassian.confluence.usercompatibility;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.usercompatibility.UserKey;
import com.atlassian.sal.usercompatibility.UserKeys;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Helper class containing utility methods for plugins which supports both Confluence 5.2 and older. In this case,
 * ConfluenceUser interface introduced in Confluence 5.2 cannot be used directly. This class contains methods which can
 * be used to ensure that a plugin can support user renaming in Confluence 5.2 and keep working on older versions of
 * Confluence.
 * <p>
 * <b>Warning: </b><br/>
 * When using this helper class, plugin is responsible for migrating existing keys from usernames to user keys
 * when Confluence has been updated to 5.2 or later.
 */
public class UserCompatibilityHelper
{
    private final static Class<?> confluenceUserClass = getClassOrNull("com.atlassian.confluence.user.ConfluenceUser");
    private final static Method userKeyGetter = getMethodOrNull(confluenceUserClass, "getKey");
    private final static Method userByKeyGetter = getMethodOrNull(UserAccessor.class, "getUserByKey", String.class);

    /**
     * Returns unique key, which e.g. can be stored in persistent storage, for given {@link User}.
     * <p>
     * For plugin which supports <b>only</b> Confluence >= 5.2 it is highly recommended to use
     * ConfluenceUser#getKey() directly.
     *
     * @param user the User
     * @return unique Key for given user or null if user was not found
     */
    public static UserKey getKeyForUser(final User user)
    {
        if (user == null)
        {
            return null;
        }

        if (isRenameUserImplemented())
        {
            final User confluenceUser = getConfluenceUser(user);

            if (confluenceUser == null)
            {
                return null;
            }

            try
            {
                final Object userKey = userKeyGetter.invoke(confluenceUser);
                return userKey != null ? new UserKey(userKey.toString()) : null;
            }
            catch (IllegalAccessException e)
            {
                throw new RuntimeException(e);
            }
            catch (InvocationTargetException e)
            {
                throw new RuntimeException(e);
            }
        }
        else
        {
            return UserKeys.getUserKeyFrom(user.getName());
        }
    }

    /**
     * Returns unique key, which e.g. can be stored in persistent storage, for given {@link User}.
     * <p>
     * For plugin which supports <b>only</b> Confluence >= 5.2 it is highly recommended to use
     * ConfluenceUser#getKey() directly.
     *
     * @param username username of the user
     * @return unique Key for given user or null if user was not found
     */
    public static UserKey getKeyForUsername(String username)
    {
        User user = getUserAccessor().getUser(username);
        return getKeyForUser(user);
    }

    /**
     * Returns unique key, which e.g. can be stored in persistent storage, for given {@link User}.
     * <p>
     * For plugin which supports <b>only</b> Confluence >= 5.2 it is highly recommended to use
     * ConfluenceUser#getKey() directly.
     *
     * @param username username of the user
     * @return unique Key for given user or null if user was not found
     */
    public static String getStringKeyForUsername(String username)
    {
        UserKey userKey = getKeyForUsername(username);
        return userKey != null ? userKey.getStringValue() : null;
    }

    private static User getConfluenceUser(User user)
    {
        User confluenceUser;
        if (confluenceUserClass.isAssignableFrom(user.getClass()))
        {
            confluenceUser = user;
        }
        else
        {
            confluenceUser = getUserAccessor().getUser(user.getName());
        }
        return confluenceUser;
    }


    /**
     * Returns {@link User} object based on specified key.
     * <p>
     * This method should be used as an opposite of {@link #getKeyForUser(User)}
     * for finding users based on string, e.g. stored in persistent storage.
     * <p>
     * For plugin which supports <b>only</b> Confluence >= 5.2 it is highly recommended to use
     * ConfluenceUserDao#getUserByKey(String) directly.
     * <p>
     * <b>Warning: </b><br/> {@link User#getName()} of returned user might not be equal to given key.
     *
     * @param key User's key
     * @return {@link User} object for given key or null if not found
     */
    public static User getUserForKey(String key)
    {
        if (key == null)
        {
            return null;
        }

        if (isRenameUserImplemented())
        {
            try
            {
                return (User) userByKeyGetter.invoke(getUserAccessor(), key);
            }
            catch (IllegalAccessException e)
            {
                throw new RuntimeException(e);
            }
            catch (InvocationTargetException e)
            {
                throw new RuntimeException(e);
            }
        }
        else
        {
            return getUserAccessor().getUser(key);
        }
    }

    private static UserAccessor getUserAccessor()
    {
        return (UserAccessor) ContainerManager.getComponent("userAccessor");
    }

    public static boolean isRenameUserImplemented()
    {
        return confluenceUserClass != null;
    }

    private static Class<?> getClassOrNull(String className)
    {
        try
        {
            return Class.forName(className);
        }
        catch (ClassNotFoundException e)
        {
            return null;
        }
    }

    private static Method getMethodOrNull(Class<?> clazz, String methodName, Class<?>... parameterTypes)
    {
        try
        {
            return clazz != null ? clazz.getMethod(methodName, parameterTypes) : null;
        }
        catch (NoSuchMethodException e)
        {
            return null;
        }
    }

}
